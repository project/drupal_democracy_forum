<?php

/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *  An array of modules to be enabled.
 */
function drupal_democracy_forum_profile_modules() {
  return array(
    // Enable required core modules.
    'block', 'filter', 'node', 'system', 'user', 'watchdog',
    
    // Enable optional core modules.
    'color', 'comment', 'forum', 'help', 'menu', 'taxonomy',
    
    // Enable contributed modules.
    'views', 'views_ui', 'votingapi', 'advpoll', 'fivestar',
    
    // Some modules are enabled in democracy_forum_profile_final().

  );
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile.
 */
function drupal_democracy_forum_profile_details() {
  return array(
    'name' => 'Drupal democracy forum',
    'description' => 'Select this profile to enable a standard democracy forum setup with vote and debate functionality.'
  );
}

/**
 * Perform any final installation tasks for this profile.
 *
 * @return
 *   An optional HTML string to display to the user on the final installation
 *   screen.
 */
function drupal_democracy_forum_profile_final() {
  
  // Install modules that require additional modules to be loaded, hence they
  // can not be listed in hook_profile_modules(). This is due to the use of
  // module specific funtions in their implementation of hook_install().
  $modules_list = array('procon', 'democracy_forum');
  drupal_install_modules($modules_list);
  
  // Rebuild menu so menu data can be accessed below.
  menu_rebuild();
  
  // Enable "Democracy forum"-link and move it to primary links.
  $item = menu_get_item(NULL, 'democracyforum');
  $type = (MENU_VISIBLE_IN_TREE | MENU_VISIBLE_IN_BREADCRUMB | MENU_MODIFIED_BY_ADMIN | MENU_MODIFIABLE_BY_ADMIN);
  $item['type'] = $type;
  $item['pid'] = variable_get('menu_primary_menu', 0);
  menu_save_item($item);
  
  // Enable Pro and Con arguments and disable comments for polls.
  variable_set('procon_enabled_advpoll_binary', 1);
  variable_set('procon_enabled_advpoll_ranking', 1);
  variable_set('comment_advpoll_binary', COMMENT_NODE_DISABLED);
  variable_set('comment_advpoll_ranking', COMMENT_NODE_DISABLED);
  
  // Enable Fivestar rating, above body, for arguments.
  variable_set('fivestar_procon_argument', 1);
  variable_set('fivestar_position_procon_argument', 'above');
  
  // Create the forum vobabulary and a default forum
  $vid = _forum_get_vid();
  drupal_execute('taxonomy_form_term', array('name' => st('Open discussion')), $vid);
  
  // Voter role with permissions
  db_query("INSERT INTO {role} (rid, name) VALUES (3, '%s')", st('voter'));
  db_query("INSERT INTO {permission} (rid, perm, tid) VALUES (3, 'cancel own vote, vote on polls, access comments, post comments, rate content, view ratings, create forum topics, access content, create procon_argument content, create proposal content, edit own proposal content, view revisions, access user profiles', 0)");
  
  // Enable "Related coucils" and "Council status" block
  db_query("INSERT INTO {blocks} VALUES ('democracy_forum', '1', 'garland', 1, 1, 'right', 0, 0, 0, '', '')");
  db_query("INSERT INTO {blocks} VALUES ('democracy_forum', '2', 'garland', 1, 0, 'right', 0, 0, 0, '', '')");

  // ------ From defaul.profile: ------------
  
  // Insert default user-defined node types into the database.
  // For a complete list of available node type attributes, refer to the node
  // type API documentation at:
  // http://api.drupal.org/api/HEAD/function/hook_node_info
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Page'),
      'module' => 'node',
      'description' => st('If you want to add a static page, like a contact page or an about page, use a page.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
    array(
      'type' => 'story',
      'name' => st('Story'),
      'module' => 'node',
      'description' => st('Stories are articles in their simplest form: they have a title, a teaser and a body, but can be extended by other modules. The teaser is part of the body too. Stories may be used as a personal blog or for news articles.'),
      'custom' => TRUE,
      'modified' => TRUE,
      'locked' => FALSE,
    ),
  );

  foreach ($types as $type) {
    $type = (object) _node_type_set_defaults($type);
    node_type_save($type);
  }

  // Default page to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));
  variable_set('comment_page', COMMENT_NODE_DISABLED);

  // Don't display date and author information for page nodes by default.
  $theme_settings = variable_get('theme_settings', array());
  $theme_settings['toggle_node_info_page'] = FALSE;
  variable_set('theme_settings', $theme_settings);
}
